# SYD0x2C-web1

## Description

Title: Flavourtext

> Some degenerate got bored of spamming refresh on all their favourite webcomics, so they decided to make this crappy RSS feed reader instead.
>
> I heard they like to keep their secrets in a flag.txt file...

## Vulnerability

The premise is that players are given a vulnerable RSS feed reader, and the objective is to get the reader to read from a malicious XML file hosted by the player. From there, they can read arbitrary files from the server (we could put a flag.txt in the root directory).

No special tooling required, a web browser and internet connection should be sufficient. In terms of knowledge required, players will need to know about XXE vulns, and the structure of an RSS feed. Knowing how to serve files over the internet will also be helpful here.

## Build and run

```
docker-compose up --build
```

Browse to `http://IPADDRESS:8045/`

## Flag

Modify `flag.txt` to change the flag.

## Solution

Included is a solution.xml file which basically cats out /flag.txt in the RSS feed display as a simple PoC.

`http://IPADDRESS/getrss.php?q=http://IPADDRESS/solution.xml`

Players must create a similar `solution.xml` file and host it on their own server and passed to `q`.
